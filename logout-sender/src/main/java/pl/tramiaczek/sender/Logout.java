package pl.tramiaczek.sender;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Set;

public interface Logout {
	Log LOG = LogFactory.getLog(Logout.class);

	void logout(Set<String> tgsIds);

	default void executeTimeConsumingOperation(String tgsId, int timeout) {
		try {
			Thread.sleep(timeout);
			LOG.info("Removing TGS: " + tgsId);
		} catch (Exception e) {
			LOG.error(e);
		}
	}
}
