package pl.tramiaczek.sender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class LogoutService {

	@Autowired
	private AsyncHttpClientLogout asyncHttpClientLogout;

	@Autowired
	private CompletableFutureLogout completableFutureLogout;

	@Scheduled(fixedDelay = 30000)
	public void executeAsyncHttpClientLogout() {
		Set<String> tgsIds = Stream.of("async_aaa", "async_bbb", "async_ccc", "async_ddd").collect(Collectors.toSet());
		asyncHttpClientLogout.logout(tgsIds);
	}

	@Scheduled(fixedDelay = 30000, initialDelay = 30000)
	public void executeCompletableFutureLogout() {
		Set<String> tgsIds = Stream.of("cf_aaa", "cf_bbb", "cf_ccc", "cf_ddd").collect(Collectors.toSet());
		completableFutureLogout.logout(tgsIds);
	}
}
