package pl.tramiaczek.sender;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.*;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.nio.conn.NHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Set;

@Component
public class AsyncHttpClientLogout implements Logout {

	private static final Log LOG = LogFactory.getLog(AsyncHttpClientLogout.class);

	private RequestConfig requestConfig;

	private NHttpClientConnectionManager clientConnectionManager;

	public AsyncHttpClientLogout(RequestConfig requestConfig, NHttpClientConnectionManager clientConnectionManager) {
		this.requestConfig = requestConfig;
		this.clientConnectionManager = clientConnectionManager;
	}

	@Override
	public void logout(Set<String> tgsIds) {
		final CloseableHttpAsyncClient httpclient;

		try {
			httpclient = HttpAsyncClientBuilder.create()
					.setDefaultRequestConfig(requestConfig)
					.setConnectionManager(clientConnectionManager)
					.setConnectionManagerShared(true)
					.addInterceptorFirst(new HttpRequestInterceptor() {
						@Override
						public void process(HttpRequest httpRequest, HttpContext httpContext) throws HttpException, IOException {
							LOG.info("Request executed: " + httpRequest.getRequestLine());
						}
					})
					.addInterceptorLast(new HttpResponseInterceptor() {
						@Override
						public void process(HttpResponse httpResponse, HttpContext httpContext) throws HttpException, IOException {
							LOG.info("Response got: " + httpResponse.getStatusLine());
						}
					}).build();
			httpclient.start();
		} catch (Exception e) {
			LOG.error("Problem while creating async client: ", e);
			return;
		}

		try {
			tgsIds.forEach(tgsId -> {
				// Execute request
				final String timestamptedTgsId = LocalDateTime.now() + "___" + tgsId;
				final HttpGet request = new HttpGet("http://nbtramiaczek:8080/logout?TGSID=" + timestamptedTgsId);
				httpclient.execute(request, new FutureCallback<org.apache.http.HttpResponse>() {
					@Override
					public void completed(org.apache.http.HttpResponse httpResponse) {
						LOG.info("Got status for TGSID: " + timestamptedTgsId + " is: " + httpResponse.getStatusLine().getStatusCode());
					}

					@Override
					public void failed(Exception e) {
						LOG.error("Failed connection for TGSID: " + timestamptedTgsId, e);
					}

					@Override
					public void cancelled() {
						LOG.info("Request cancelled for TGSID: " + timestamptedTgsId);
					}
				});

				executeTimeConsumingOperation(timestamptedTgsId, 1000);
			});
		} catch (Exception e) {
			LOG.error("Problem while creating connection manager", e);
		} finally {
			try {
				if (httpclient.isRunning()) {
					httpclient.close();
				}
			} catch (Exception e) {
				LOG.error("Problem while closing connection: ", e);
			}
		}
	}
}
