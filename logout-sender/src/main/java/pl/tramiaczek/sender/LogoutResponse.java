package pl.tramiaczek.sender;

public class LogoutResponse {
	String tgsId;
	boolean status;

	public LogoutResponse() {
	}

	LogoutResponse(String tgsId, boolean status) {
		this.tgsId = tgsId;
		this.status = status;
	}

	public String getTgsId() {
		return tgsId;
	}

	public void setTgsId(String tgsId) {
		this.tgsId = tgsId;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
}