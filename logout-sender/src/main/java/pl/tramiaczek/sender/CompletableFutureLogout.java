package pl.tramiaczek.sender;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Set;
import java.util.concurrent.CompletableFuture;

@Component
public class CompletableFutureLogout implements Logout {

	private static final Log LOG = LogFactory.getLog(CompletableFutureLogout.class);

	public void logout(Set<String> tgsIds) {
		LOG.info("Do some heavy work...");
		tgsIds.forEach(tgsId -> {
					CompletableFuture.supplyAsync(
							() -> {
								RestTemplate restTemplate = new RestTemplate();
								return restTemplate.getForObject("http://localhost:8080/logout?TGSID=" + tgsId, LogoutResponse.class);
							}).thenAccept(result -> {
						LOG.info("Response: " + result);
						LOG.info("Removing tgsId: " + tgsId);
					});

					// time consuming operation
					executeTimeConsumingOperation(tgsId, 10);
				}
		);
		LOG.info("Not waiting for completion...");
	}
}
