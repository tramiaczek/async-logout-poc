package pl.tramiaczek.sender;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.nio.conn.PoolingNHttpClientConnectionManager;
import org.apache.http.impl.nio.reactor.DefaultConnectingIOReactor;
import org.apache.http.impl.nio.reactor.IOReactorConfig;
import org.apache.http.nio.reactor.ConnectingIOReactor;
import org.apache.http.nio.reactor.IOReactorException;
import org.apache.http.nio.reactor.IOReactorExceptionHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@EnableScheduling
@SpringBootApplication
public class LogoutSenderApplication {

	private static final Log LOG = LogFactory.getLog(LogoutSenderApplication.class);

	@Bean
	public RequestConfig createConnectionConfig() {
		LOG.info("Creating request config...");
		return RequestConfig.custom()
				.setConnectionRequestTimeout(3000)
				.setConnectTimeout(5000)
				.setSocketTimeout(7000).build();
	}

	@Bean
	public PoolingNHttpClientConnectionManager createPoolingConnectionManager() throws IOReactorException {
		LOG.info("Creating connection manager...");
		IOReactorConfig config = IOReactorConfig.DEFAULT;
		ConnectingIOReactor ioReactor = new DefaultConnectingIOReactor(config);
		((DefaultConnectingIOReactor) ioReactor).setExceptionHandler(new IOReactorExceptionHandler() {
			@Override
			public boolean handle(IOException e) {
				LOG.error("Reactor exception: ", e);
				return false;
			}

			@Override
			public boolean handle(RuntimeException e) {
				LOG.error("Reactor exception: ", e);
				return false;
			}
		});

		PoolingNHttpClientConnectionManager cm = new PoolingNHttpClientConnectionManager(ioReactor);
		cm.setMaxTotal(50);
		cm.setDefaultMaxPerRoute(10);

		return cm;
	}

	public static void main(String[] args) {
		SpringApplication.run(LogoutSenderApplication.class, args);
	}
}
