package pl.tramiaczek.resources;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogoutEndpointApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogoutEndpointApplication.class, args);
	}
}
