package pl.tramiaczek.resources;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LogoutResource {

	private static final Log LOG = LogFactory.getLog(LogoutResource.class.getName());

	@GetMapping(path = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
	public LogoutResponse logout(@RequestParam(value = "TGSID") String tgsId) {
		LOG.info("Logging out session: " + tgsId);
		return new LogoutResponse(tgsId, true);
	}

	static class LogoutResponse {
		String tgsId;
		boolean status;

		public LogoutResponse() {
		}

		LogoutResponse(String tgsId, boolean status) {
			this.tgsId = tgsId;
			this.status = status;
		}

		public String getTgsId() {
			return tgsId;
		}

		public void setTgsId(String tgsId) {
			this.tgsId = tgsId;
		}

		public boolean isStatus() {
			return status;
		}

		public void setStatus(boolean status) {
			this.status = status;
		}
	}
}
